# Ansible playbooks for development machine environment setup

## The basic setup of a new system should be ...

From a fresh system run ...

1. Setup prerequisites as below
1. Run the bootstrap-devel.yml playbook against local target
```
$> ansible-playbook plays/bootstrap-devel.yml --limit local
```
1. Run the bootstrap-remote.yml playbook against remote targets where you want your authorized key stored. Requires sudo.
```
$> ansible-playbook plays/bootstrap-remote.yml --limit <host group>
```
1. If you wish to setup a dev environment on the remotes -
```
$> ansible-playbook plays/bootstrap-devel.yml --limit <host group>
```

## Prerequisites on control host

### Install Ansible and Python

```
$> sudo apt-get install python3 ansible
```

## Playbooks

* `plays/bootstrap-devel.yml` - Targets a host for installation of development environment and ssh keygen
* `plays/bootstrap-remote.yml` - Targets a remote host for installation of authorized_keys and ansible setup

## Running bootstrap-remote

*Note - shouldn't run this on local*

```
$> ansible-playbook plays/bootstrap-remote.yml --limit <host group> 
```
